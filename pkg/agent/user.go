package agent

import (
	"regexp"
	"strings"
)

type UserList []*User

func (u UserList) Len() int      { return len(u) }
func (u UserList) Swap(i, j int) { u[i], u[j] = u[j], u[i] }
func (u UserList) Less(i, j int) bool {
	return strings.ToLower(u[i].N) < strings.ToLower(u[j].N)
}

type User struct {
	ID int
	N  string // Nickname
	C  int    // Channel
}

var nickRegexp = regexp.MustCompile(`[^a-zA-Z0-9_\-!@#$%^&*+=,./?]+`)

func Nickname(nick string) string {
	nick = nickRegexp.ReplaceAllString(nick, "")
	if len(nick) > 10 {
		nick = nick[:10]
	} else if nick == "" {
		nick = "Anonymous"
	}

	return nick
}
