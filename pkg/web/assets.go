// +build !embed

package web

import "github.com/omeid/go-resources/live"

func init() {
	assets = live.Dir("./public")
}
