var displayStats = false;

var socket = null;
var ReconnectDelay = 0;
var reconnectTimeout;
var connected;

var voice = false;
var ptt = false;
var nickname = "Anonymous";

var numConnections = 3;
var peerConnections = [];
var RTCConstraints = {
    audio: {
        channelCount: 2,
        autoGainControl: false,
        echoCancellation: false,
        noiseSuppression: false,
        sampleRate: 48000,
        sampleSize: 16,
        volume: 1.0
    },
    video: false
};
var RTCOfferOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 0,
    voiceActivityDetection: false
};
var RTCICEServers = [{urls: 'stun:stun.l.google.com:19302'}];
var audioTrack;

var shownPTTHelp = false;
var menuOpen = false;

var lastPing = 0;
var userPing = -1;

var allChannels;
var allChannelsSorted;
var channelBuffers = [];
var textChannel = -1;
var voiceChannel = -1;
var disableVoice = false;
var voiceCompatibilityNotice = "Sorry, your browser does not support WebRTC. This is required join voice channels. Firefox (recommended) and Chrome support WebRTC.";

var channelList = '';
var userList = '';
var userListStatus = 'Loading...';

var MessageBinary = 2;
var MessagePing = 101;
var MessagePong = 102;
var MessageCall = 103;
var MessageAnswer = 104;
var MessageConnect = 110;
var MessageJoin = 111;
var MessageQuit = 112;
var MessageNick = 113;
var MessageTopic = 114;
var MessageAction = 115;
var MessageDisconnect = 119;
var MessageChat = 120;
var MessageTypingStart = 121;
var MessageTypingStop = 122;
var MessageTransmitStart = 123;
var MessageTransmitStop = 124;
var MessageServers = 130;
var MessageChannels = 131;
var MessageUsers = 132;

var ChannelUnknown = 0;
var ChannelText = 1;
var ChannelVoice = 2;

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

var clickEventType = ((document.ontouchstart !== null) ? 'mousedown' : 'touchstart');

$(document).keydown(HandleInput);
$(document).keyup(HandleInput);

function HandleInput(e) {
    if (e.which == 119) {
        if (e.type == "keydown") {
            StartPTT();
        } else if (e.type == "keyup") {
            StopPTT();
        }

        e.preventDefault();
    } else if (e.which >= 32 && e.which <= 126) {
        $("#chatinput").focus();
        // Do not prevent default
    } else if ((e.which == 13 || e.which == 176) && e.type == "keydown" && !e.shiftKey) {
        if (!$("#chatinput").is(":focus")) {
            return;
        }

        if ($("#chatinput").val() != "") {
            if ($("#chatinput").val() == "/debugstats") {
                enableStats();
            } else {
                writeSocket({T: MessageChat, C: textChannel, M: btoa($("#chatinput").val())});
            }
        }

        $("#chatinput").val('');
        e.preventDefault();
    }
}

$(document).ready(function () {
    // Confirm voice chat support
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia || !window.RTCPeerConnection) {
        alert(voiceCompatibilityNotice);
        Log(voiceCompatibilityNotice);

        disableVoice = true;
    }

    $("#voiceptt").on(clickEventType, function (e) {
        StartPTT();

        return false;
    });

    $("#voiceptt").on("mouseup touchend", function (e) {
        StopPTT();

        return false;
    });

    $(document).on("mouseup", function (e) {
        StopPTT();

        return false;
    });

    $(document).on("touchend", function (e) {
        StopPTT();

        return false;
    });

    $("#chatinputcontainer,#voicepttcontainer").on(clickEventType, function (e) {
        $("#chatinput").focus();

        return false;
    });

    $("#chatinput").on(clickEventType, function (e) {
        $("#chatinput").focus();

        return false;
    });

    $(".menucontainer,.mobilemenucontainer").on(clickEventType, function (e) {
        menuOpen = !menuOpen;
        if (menuOpen) {
            $(".wrapper").css('display', 'none');
            $(".mobilewrapper").css('display', 'grid');
        } else {
            $(".mobilewrapper").css('display', 'none');
            $(".wrapper").css('display', 'grid');
        }

        updateHeader();

        return false;
    });

    window.setInterval(() => {
        if (!webSocketReady()) {
            return;
        }

        pingServer();
    }, 15000);

    nickname = prompt("What is your name?", nickname);
    Connect();
});

function createPeerConnection(id) {
    var pc = new RTCPeerConnection({
        iceServers: RTCICEServers
    });
    pc.onicecandidate = event => {
        if (event.candidate === null) {

        }
    };

    if (id > 0) {
        pc.addTransceiver('audio', {'direction': 'sendrecv'}); // TODO Workaround. Should be recvonly. See pion/webrtc#1199
    }

    var pcid = id;
    pc.ontrack = function (event) {
        console.log(`PC ${pcid} onTrack ${event.streams.length} ${event.streams[0].id} ${event.streams[0].getAudioTracks().length}`);

        if (pcid == 0) {
            pc.addTransceiver(event.streams[0].getAudioTracks()[0], {'direction': 'sendrecv'});
        }

        $("#hidden").append('<audio id="audiostream' + pcid + '"></audio>');

        var el = document.getElementById('audiostream' + pcid);
        el.autoplay = true;
        el.srcObject = event.streams[0];

        if (pcid == 0) {
            if (!shownPTTHelp) {
                shownPTTHelp = true;

                Log("Note: Push-to-talk is bound to &lt;F8&gt;");
            }

            $('#voicepttcontainer').css('display', 'block');
            updateStatus();
        }
    };

    if (id == 0) {
        navigator.mediaDevices.getUserMedia(RTCConstraints).then(localStream => {
            var audioTracks = localStream.getAudioTracks();
            if (audioTracks.length > 0) {
                console.log(`PC ${id} Using Audio device: ${audioTracks[0].label} - tracks available: ${audioTracks}`);
            } else {
                console.log(`PC ${id} found no audio devices`);
            }

            audioTrack = audioTracks[0];

            pc.addTrack(audioTrack);
            pc.getSenders()[0].replaceTrack(null).then(function () {
                pc.createOffer(RTCOfferOptions)
                    .then(onRTCDescription(id), onRTCDescriptionError(id));
            });
        }).catch(FatalAudioError);
    } else {
        pc.createOffer(RTCOfferOptions)
            .then(onRTCDescription(id), onRTCDescriptionError(id));
    }

    return pc;
}

function onRTCDescriptionError(id) {
    return function (error) {
        console.log(`Failed to create/set session description: ${error.toString()}`);
    }
}

function onRTCDescription(id) {
    return function (desc) {
        console.log(`PC ${id} Offer received \n${desc.sdp}`);

        if (peerConnections.length < numConnections) {
            peerConnections.push(createPeerConnection(peerConnections.length));
        }

        if (peerConnections.length < id) {
            return;
        }

        peerConnections[id].setLocalDescription(desc)
            .then(() => {
                desc.sdp = maybePreferCodec(desc.sdp, 'audio', 'send', 'opus/48000');
                desc.sdp = maybePreferCodec(desc.sdp, 'audio', 'receive', 'opus/48000');

                console.log(`PC ${id} onRTCDescription`);

                wpc(id, MessageCall, desc.sdp);
            }, onRTCDescriptionError);
    }
}

function Connect() {
    reconnectTimeout = null;
    if (webSocketReady() || ReconnectDelay === -1) {
        return;
    }

    userPing = -1;
    userListStatus = 'Connecting...';
    updateStatus();

    Log("Connecting...");

    var loc = window.location, wsurl, pathname;
    if (loc.protocol === "https:") {
        wsurl = "wss:";
    } else {
        wsurl = "ws:";
    }
    if (loc.pathname && loc.pathname !== "") {
        pathname = loc.pathname;
    } else {
        pathname = "/";
    }
    wsurl += "//" + loc.host + pathname + "w";

    socket = new WebSocket(wsurl);
    socket.onerror = function (e) {
        Log("Connection error");
        console.log(e);
        QuitVoice();
    };
    socket.onopen = function (e) {
        if (reconnectTimeout != null) {
            clearTimeout(reconnectTimeout);
        }

        // TODO Update after receiving channels
        updateHeader();

        userListStatus = "";

        w(MessageNick, nickname);

        pingServer();
    };
    socket.onmessage = function (e) {
        if (ReconnectDelay > 0) {
            ReconnectDelay = 0;
        }

        try {
            if (typeof e.data === "string") {
                var p = JSON.parse(e.data);
                if (p.M !== undefined) {
                    p.M = atob(p.M);
                }

                console.log("Read ", p);

                if (p.T == MessagePong) {
                    if (parseInt(p.M, 10) == lastPing) {
                        userPing = Date.now() - lastPing;

                        updateStatus();
                    }
                } else if (p.T == MessageAnswer) {
                    if (p.PC === undefined || p.PC > peerConnections.length) {
                        return;
                    }

                    var pc = peerConnections[p.PC];
                    pc.setRemoteDescription(new RTCSessionDescription({type: 'answer', sdp: p.M}));
                } else if (p.T == MessageConnect) {
                    if (p.N === undefined) {
                        return;
                    }

                    Log(escapeEntities(p.N) + " connected");
                } else if (p.T == MessageJoin) {
                    if (p.C === undefined || p.N === undefined) {
                        return;
                    }

                    Log(escapeEntities(p.N) + " joined " + allChannels[p.C].Name);
                } else if (p.T == MessageQuit) {
                    if (p.C === undefined || p.N === undefined) {
                        return;
                    }

                    Log(escapeEntities(p.N) + " quit " + allChannels[p.C].Name);
                } else if (p.T == MessageDisconnect) {
                    if (p.N === undefined) {
                        return;
                    }

                    Log(escapeEntities(p.N) + " disconnected");
                } else if (p.T == MessageNick) {
                    if (p.N === undefined) {
                        return;
                    }

                    Log(escapeEntities(p.N) + " is now known as " + escapeEntities(p.M));
                } else if (p.T == MessageChat) {
                    if (p.N === undefined) {
                        return;
                    }

                    LogChannel(p.C, "&lt;" + escapeEntities(p.N) + "&gt; " + p.M);
                } else if (p.T == MessageChannels) {
                    allChannelsSorted = JSON.parse(p.M);

                    allChannels = [];
                    for (let i = 0; i < allChannelsSorted.length; i++) {
                        allChannels[allChannelsSorted[i].ID] = allChannelsSorted[i];
                    }

                    updateChannelList();
                } else if (p.T == MessageUsers) {
                    var userListNew = '<ul>';

                    var u = JSON.parse(p.M);
                    userListNew += '<li><b>' + u.length + ' user' + (u.length != 1 ? 's' : '') + '</b></li>';
                    for (let i = 0; i < u.length; i++) {
                        userListNew += '<li>' + u[i].N + '</li>';
                    }

                    userListNew += '</ul>';

                    userList = userListNew;

                    var userListVoiceNew;
                    var u = JSON.parse(p.M);
                    for (let ci in allChannels) {
                        $(".channelvoiceusers" + ci).html('');

                        userListVoiceNew = '<ul>';
                        ;
                        for (let i = 0; i < u.length; i++) {
                            if (u[i].C != ci) {
                                continue;
                            }

                            userListVoiceNew += '<li>';
                            if (voice && u[i].C == voiceChannel) {
                                userListVoiceNew += '<span id="voiceindicator' + u[i].ID + '"><div class="voiceinactive">&#128264;</div></span> ';
                            }
                            userListVoiceNew += u[i].N + '</li>';
                        }

                        if (userListVoiceNew != '<ul>') {
                            userListVoiceNew += '<li style="margin: 0 !important;height: 7px;">&nbsp;</li></ul>';

                            $(".channelvoiceusers" + ci).each(function (index) {
                                $(this).html(userListVoiceNew);
                            });
                        }
                    }

                    updateUserList();
                } else if (p.T == MessageTransmitStart || p.T == MessageTransmitStop) {
                    if (!voice || p.S === undefined) {
                        return;
                    }

                    $("#voiceindicator" + p.S).html(p.T == MessageTransmitStart ? '<div class="voiceactive">&#128266;</div>' : '<div class="voiceinactive">&#128264;</div>');
                }
            } else {
                // TODO Binary data
            }
        } catch (e) {
            console.log(e);
        }
    };
    socket.onclose = function (e) {
        connected = false;
        Log("Disconnected");
        QuitVoice();

        if (ReconnectDelay < 0 || reconnectTimeout != null) {
            return;
        }

        ReconnectDelay += (ReconnectDelay * 2) + 1;

        var waitTime = ReconnectDelay;
        console.log("Reconnecting in " + ReconnectDelay + " seconds...");
        reconnectTimeout = setTimeout(Connect, waitTime * 1000);

        if (ReconnectDelay > 10) {
            ReconnectDelay = 10;
        }
    };
}

function pingServer() {
    lastPing = Date.now();
    w(MessagePing, lastPing);
}

function webSocketReady() {
    return (socket !== null && socket.readyState === 1);
}

function waitForSocketConnection(socket, callback) {
    setTimeout(function () {
        if (webSocketReady()) {
            if (callback != null) {
                callback();
            }
        } else {
            waitForSocketConnection(socket, callback);
        }
    }, 250);
}

function Log(msg) {
    LogChannel(textChannel, msg);
}

function LogChannel(ch, msg) {
    if (msg.charAt && msg.charAt(0) != '<' && msg.charAt(0) != '&' && msg.charAt(1) != '&' && msg.charAt(2) != 't' && msg.charAt(3) != ';') {
        msg = '* ' + msg;
    }

    var date = new Date();
    msg = date.getHours() + ":" + (date.getMinutes() < 10 ? "0" : "") + date.getMinutes() + " " + msg + "\n";

    if (ch >= 0) {
        if (channelBuffers[ch] === undefined) {
            channelBuffers[ch] = msg;
        } else {
            channelBuffers[ch] += '<br>' + msg;
        }

        if (ch != textChannel) {
            return;
        }
    }

    if ($('.buffer').html().trim() != "") {
        $('.buffer').append("<br>");
    }
    $('.buffer').append(msg);
    $('.buffer').scrollTop($('.buffer').prop("scrollHeight"));
}

function FatalAudioError(msg) {
    QuitVoice();

    alert(msg);
    Log(msg);
}

function StartPTT() {
    if (ptt || !voice || peerConnections.length == 0) {
        return;
    }

    ptt = true;

    $("#voiceptt").html('Transmitting...');

    var senders = peerConnections[0].getSenders();
    if (senders.length == 0) {
        return;
    }
    senders[0].replaceTrack(audioTrack);

    updateStatus();
}

function StopPTT() {
    if (!ptt || !voice || peerConnections.length == 0) {
        return;
    }

    ptt = false;

    $("#voiceptt").html('Push-To-Talk');

    var senders = peerConnections[0].getSenders();
    if (senders.length == 0) {
        return;
    }
    senders[0].replaceTrack(null);

    updateStatus();
}

function ToggleVoiceChannel(channelID) {
    if (voiceChannel == channelID) {
        QuitVoice();
        return false;
    }

    if (disableVoice) {
        alert(voiceCompatibilityNotice);
        return false;
    }

    voiceChannel = channelID;
    JoinVoice(voiceChannel);
    return false;
}

function JoinText(channelID) {
    textChannel = parseInt(channelID);
    updateHeader();
    updateBuffer();

    for (let i in allChannels) {
        $(".jointext" + i).each(function (index) {
            $(this).removeClass('menulink');
            $(this).removeClass('menulinkinactive');
            $(this).addClass(i != textChannel ? 'menulink' : 'menulinkinactive');
        });
    }
}

function JoinVoice(channelID) {
    if (!webSocketReady()) {
        return;
    }

    voiceChannel = parseInt(channelID);
    for (let i in allChannels) {
        $(".joinvoice" + i).each(function (index) {
            $(this).removeClass('menulink');
            $(this).removeClass('menulinkinactive');
            $(this).addClass(i != voiceChannel ? 'menulink' : 'menulinkinactive');
        });
        $("#voicestatus").each(function (index) {
            $(this).css('display', 'block');
        });
    }

    voice = true;

    if (peerConnections.length == 0) {
        peerConnections.push(createPeerConnection(0));
    }

    writeSocket({T: MessageJoin, C: parseInt(channelID)});
}

function QuitVoice() {
    for (let i in allChannels) {
        $(".joinvoice" + i).each(function (index) {
            $(this).removeClass('menulink');
            $(this).removeClass('menulinkinactive');
            $(this).addClass('menulink');
        });
        $("#voicestatus").each(function (index) {
            $(this).css('display', 'none');
        });
    }

    voice = false;
    voiceChannel = 0;

    w(MessageQuit, "");

    peerConnections.forEach(function (pc) {
        pc.close();
    });
    peerConnections = [];

    $('#voicepttcontainer').css('display', 'none');

    updateStatus();
}

function updateHeader() {
    if (menuOpen) {
        $(".mainheader").html('Menu');
        $(".subheader").html('');
    } else if (allChannels !== undefined) {
        $(".mainheader").html(allChannels[textChannel].Name);
        $(".subheader").html(allChannels[textChannel].Topic);
    } else {
        $(".mainheader").html('Loading...');
        $(".subheader").html('');
    }
}

function updateBuffer() {
    if (textChannel == -1 || channelBuffers[textChannel] === undefined) {
        $('.buffer').html('');
        return;
    }

    $('.buffer').html(channelBuffers[textChannel]);
}

function updateStatus() {
    var out = '';
    if (userPing >= 0) {
        out += userPing + 'ms ping';
    }

    if (userListStatus != '') {
        if (out != '') {
            out += '<br>';
        }

        out += userListStatus;
    }

    $('#userstatus').html(out);
}

function updateChannelList() {
    var c;
    var channelListNew = '<ul class="sidemenu">';

    for (let i in allChannelsSorted) {
        c = allChannelsSorted[i];

        if (c.Type != ChannelText) {
            continue;
        }

        if (textChannel == -1) {
            JoinText(parseInt(c.ID));
        }

        channelListNew += '<li class="sideheading channeltext' + c.ID + '">';
        channelListNew += '<a href="#" onclick="JoinText(' + parseInt(c.ID) + ');return false;" ontouchstart="JoinText(' + parseInt(c.ID) + ');return false;" class="menulink jointext' + c.ID + '">&#128240; ';
        channelListNew += c.Name;
        channelListNew += '</a>';
        channelListNew += '</li>';
    }

    channelListNew += '<li>&nbsp;</li>';
    for (let i in allChannelsSorted) {
        c = allChannelsSorted[i];

        if (c.Type != ChannelVoice) {
            continue;
        }

        channelListNew += '<li class="sideheading channelvoice' + c.ID + '">';
        channelListNew += '<a href="#" onclick="JoinVoice(' + parseInt(c.ID) + ');return false;" ontouchstart="JoinVoice(' + parseInt(c.ID) + ');return false;" class="menulink joinvoice' + c.ID + '">&#128266; ';
        channelListNew += c.Name;
        channelListNew += '</a>';
        channelListNew += '</li>';
        channelListNew += '<li class="channelvoiceusers' + c.ID + '"></li>';
    }

    channelListNew += '</ul>';

    channelList = channelListNew;

    $(".sideleft,.mobilesideleft").html(channelList);
    JoinText(textChannel);
}

function updateUserList() {
    $('.sideright,.mobilesideright').html(userList);
}

function w(t, m) {
    writeSocket({T: t, M: btoa(m)});
}

function wpc(pc, t, m) {
    writeSocket({PC: parseInt(pc), T: t, M: btoa(m)});
}

function writeSocket(p) {
    console.log("Write ", p);

    if (!webSocketReady()) {
        return;
    }

    socket.send(JSON.stringify(p))
}

function escapeEntitiesCallback(tag) {
    return tagsToReplace[tag] || tag;
}

function escapeEntities(str) {
    return str.replace(/[&<>]/g, escapeEntitiesCallback);
}

function enableStats() {
    if (displayStats) {
        return;
    }

    displayStats = true;

    window.setInterval(() => {
        var i;
        for (i = 0; i < peerConnections.length; i++) {
            var senders = peerConnections[i].getSenders();

            var j;
            for (j = 0; j < senders.length; j++) {
                senders[j].getStats().then(stats => {
                    let statsOutput = "";

                    stats.forEach(report => {
                        if (report.type == "local-candidate" || report.type == "remote-candidate") {
                            return;
                        } else if (report.type == "candidate-pair" && (report.bytesSent == 0 && report.bytesReceived == 0)) {
                            return;
                        }

                        statsOutput += `<b>PC${i}S${j} Report: ${report.type}</b>\n<strong>ID:</strong> ${report.id}<br>\n` +
                            `<strong>Timestamp:</strong> ${report.timestamp}<br>\n`;

                        Object.keys(report).forEach(statName => {
                            if (statName !== "id" && statName !== "timestamp" && statName !== "type") {
                                statsOutput += `<strong>${statName}:</strong> ${report[statName]}<br>\n`;
                            }
                        });
                    });

                    if (statsOutput != "") {
                        Log(statsOutput);
                    }
                });
            }
        }
    }, 1000);

    Log("Debug stats enabled");
}

// Copied from AppRTC's sdputils.js:

// Sets |codec| as the default |type| codec if it's present.
// The format of |codec| is 'NAME/RATE', e.g. 'opus/48000'.
function maybePreferCodec(sdp, type, dir, codec) {
    const str = `${type} ${dir} codec`;
    if (codec === '') {
        console.log(`No preference on ${str}.`);
        return sdp;
    }

    console.log(`Prefer ${str}: ${codec}`);

    const sdpLines = sdp.split('\r\n');

    // Search for m line.
    const mLineIndex = findLine(sdpLines, 'm=', type);
    if (mLineIndex === null) {
        return sdp;
    }

    // If the codec is available, set it as the default in m line.
    const codecIndex = findLine(sdpLines, 'a=rtpmap', codec);
    console.log('codecIndex', codecIndex);
    if (codecIndex) {
        const payload = getCodecPayloadType(sdpLines[codecIndex]);
        if (payload) {
            sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], payload);
        }
    }

    sdp = sdpLines.join('\r\n');
    return sdp;
}

// Find the line in sdpLines that starts with |prefix|, and, if specified,
// contains |substr| (case-insensitive search).
function findLine(sdpLines, prefix, substr) {
    return findLineInRange(sdpLines, 0, -1, prefix, substr);
}

// Find the line in sdpLines[startLine...endLine - 1] that starts with |prefix|
// and, if specified, contains |substr| (case-insensitive search).
function findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
    const realEndLine = endLine !== -1 ? endLine : sdpLines.length;
    for (let i = startLine; i < realEndLine; ++i) {
        if (sdpLines[i].indexOf(prefix) === 0) {
            if (!substr ||
                sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                return i;
            }
        }
    }
    return null;
}

// Gets the codec payload type from an a=rtpmap:X line.
function getCodecPayloadType(sdpLine) {
    const pattern = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+');
    const result = sdpLine.match(pattern);
    return (result && result.length === 2) ? result[1] : null;
}

// Returns a new m= line with the specified codec as the first one.
function setDefaultCodec(mLine, payload) {
    const elements = mLine.split(' ');

    // Just copy the first three parameters; codec order starts on fourth.
    const newLine = elements.slice(0, 3);

    // Put target payload first and copy in the rest.
    newLine.push(payload);
    for (let i = 3; i < elements.length; i++) {
        if (elements[i] !== payload) {
            newLine.push(elements[i]);
        }
    }
    return newLine.join(' ');
}
