package audio

const (
	ClockRate = 48 // KHz
	FrameTime = 20 // ms
	Channels  = 2  // Stereo

	Samples = ClockRate * FrameTime
)
