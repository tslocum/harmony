package main

import (
	"io/ioutil"
	"os"
	"strings"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

type ChannelConfig struct {
	Type  string
	Topic string
}

type Config struct {
	WebAddress string
	WebPath    string
	Channels   map[string]ChannelConfig
}

var config = Config{WebPath: "/"}

func readconfig(configPath string) error {
	if configPath == "" {
		return errors.New("file unspecified")
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		return errors.New("file not found")
	}

	configData, err := ioutil.ReadFile(configPath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(configData, &config)
	if err != nil {
		return err
	}

	err = validateConfig()
	if err != nil {
		return err
	}

	if len(config.Channels) == 0 {
		addPlaceholderchannels()
	}

	newChannels := make(map[string]ChannelConfig)
	for channelName, ch := range config.Channels {
		newChannels[strings.TrimSpace(channelName)] = ChannelConfig{Type: strings.TrimSpace(ch.Type), Topic: strings.TrimSpace(ch.Topic)}
	}
	config.Channels = newChannels

	return nil
}

func validateConfig() error {
	if config.Channels == nil {
		config.Channels = make(map[string]ChannelConfig)
	}

	if config.WebAddress == "" {
		return errors.New("WebAddress is required")
	}

	return nil
}

func addPlaceholderchannels() {
	config.Channels["Lobby-Text"] = ChannelConfig{Type: "text"}
	config.Channels["Lobby-Voice"] = ChannelConfig{Type: "voice"}
}
