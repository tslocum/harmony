package main

import (
	"flag"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"path"
	"strings"

	"gitlab.com/tslocum/harmony/pkg/agent"
	"gitlab.com/tslocum/harmony/pkg/web"
)

var (
	configPath   string
	debugAddress string
)

func main() {
	flag.StringVar(&configPath, "config", "", "path to configuration file")
	flag.StringVar(&debugAddress, "debug-address", "", "address to serve debug info")
	flag.Parse()

	if debugAddress != "" {
		go func() {
			log.Fatal(http.ListenAndServe(debugAddress, nil))
		}()
	}

	if configPath == "" {
		homedir, err := os.UserHomeDir()
		if err == nil && homedir != "" {
			configPath = path.Join(homedir, ".config", "harmony-server", "config.yaml")
		}
	}

	err := readconfig(configPath)
	if err != nil {
		log.Fatalf("Failed to read configuration file %s: %v\nSee HOSTING.md for information on how to configure harmony-server", configPath, err)
	}

	w := web.NewWebInterface(config.WebAddress, config.WebPath)

	for channelName, ch := range config.Channels {
		t := agent.ChannelText
		if ch.Type != "" && strings.ToLower(ch.Type)[0] == 'v' {
			t = agent.ChannelVoice
		}

		w.AddChannel(t, channelName, ch.Topic)
	}

	log.Println("harmony-server started")

	_ = w
	select {}
}
