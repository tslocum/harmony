module gitlab.com/tslocum/harmony

go 1.13

require (
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/lucas-clemente/quic-go v0.16.0 // indirect
	github.com/omeid/go-resources v0.0.0-20200113210624-eb442c910d63
	github.com/pion/rtp v1.5.4
	github.com/pion/sdp/v2 v2.3.8 // indirect
	github.com/pion/srtp v1.3.4 // indirect
	github.com/pion/stun v0.3.5 // indirect
	github.com/pion/webrtc/v2 v2.2.14
	github.com/pkg/errors v0.9.1
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3 // indirect
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200602100848-8d3cce7afc34 // indirect
	google.golang.org/protobuf v1.24.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200602140019-6ec2bf8d378b
)
