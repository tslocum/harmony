# harmony
[![CI status](https://gitlab.com/tslocum/harmony/badges/master/pipeline.svg)](https://gitlab.com/tslocum/harmony/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Voice and text communications platform

## Features

- Low-latency voice chat using [Opus](https://en.wikipedia.org/wiki/Opus_%28audio_format%29)
- Rich text chat using [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Screenshot

[![](https://harmony.rocketnine.space/static/screenshot_v010_2.png)](https://harmony.rocketnine.space/static/screenshot_v010_2.png)

## Web client

The only client currently implemented is a web interface (located in pkg/web)
hosted by the server.

## Server

[**Download harmony-server**](https://harmony.rocketnine.space/download/?sort=name&order=desc)

The server (located in cmd/harmony-server) passes voice and text communications
between clients.

See [HOSTING.md](https://gitlab.com/tslocum/harmony/blob/master/HOSTING.md) for information
 on hosting a server.

See [PROTOCOL.md](https://gitlab.com/tslocum/harmony/blob/master/PROTOCOL.md) for information
 on the harmony protocol.

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/harmony/issues).

## Libraries

The following libraries are used to build harmony:

* [pion/webrtc](https://github.com/pion/webrtc) - WebRTC connections and audio 
* [gorilla/websocket](https://github.com/gorilla/websocket) - WebSocket connections
