harmony protocol v0.0.1

# Introduction

The harmony protocol uses [JSON](https://en.wikipedia.org/wiki/JSON) as its
message format.

Servers are assigned IDs upon creation starting at 10. This ID may be used to
specify which instance to join or connect to.  Example join link:

https://harmony.rocketnine.space/join/44

When an ID is not specified, the first server instance (ID 10) is assumed.

Specifying a server ID is only required when initially connecting.

# Voice communication overview

[WebRTC](https://webrtc.org) facilitates voice communication. When a client
requests to join a voice channel, the server is sent one or more `Call`
commands to initiate audio connections. Once at least one connection is
established, the client will send the server a `Join` command with a voice
channel ID.

Current implementations establish three audio connections and route incoming
audio over the first available connection. This may change.

# Channel types

| # | Description |
| --- | --- |
| 1 | Text |
| 2 | Voice |

# Messages

| # | Name | Description | Syntax |
| --- | --- | --- | --- |
| 110 | Connect | Connect to a server | `{ T: 110, S: <OPTIONAL SERVERID>, U: "<USERNAME>", P: "<OPTIONAL PASSWORD>" }` Request to connect<br> `{ T: 110, U: "<USERNAME>" }` Notification of user connecting |
| 101 | Ping | Request to ensure connection is still active | `{ T: 101, M: "<TIMESTAMP>" }` |
| 102 | Pong | Reply ensuring connection is still active | `{ T: 102, M: "<REQUEST TIMESTAMP>" }` |
| 120 | Chat | Chat message | `{ T: 120, C: <CHANNEL ID>, M: "<MESSAGE>" }` Request to send message<br> `{ T: 120, U: "<USERNAME>", M: "<MESSAGE>" }` Notification of message |
| 103 | Call | Initiate voice connection with server | `{ T: 103, M: "<WEBRTC LOCAL SDP>" }` |
| 104 | Answer | Reply to initiate voice connection with server | `{ T: 104, M: "<WEBRTC REMOTE SDP>" }` |
| 111 | Join | Join a voice channel | `{ T: 111, C: <CHANNEL ID>, P: "<OPTIONAL PASSWORD>" }` Request to join<br> `{ T: 111, C: <CHANNEL ID>, U: "<USERNAME>" }` Notification of user joining |
| 112 | Quit | Quit a voice channel | `{ T: 112, C: <CHANNEL ID> }` Request to quit<br> `{ T: 112, C: <CHANNEL ID>, U: "<USERNAME>" }` Notification of user quitting |
| 123 | TransmitStart | Notification of voice activity starting | `{ T: 123, S: <CLIENT ID> }` |
| 124 | TransmitStop | Notification of voice activity stopping | `{ T: 124, S: <CLIENT ID> }` |
| 131 | Channels | Channel list | `{ T: 131, M: "[{\"ID\": <ID>,\"Type\": <TYPE>,\"Name\": \"<NAME>\",\"Topic\": \"<TOPIC>\"}]" }` |
| 132 | Users | User list | `{ T: 132, M: "[{\"ID\": <ID>,\"N\": \"<USERNAME>\",\"C\": \"<VOICE CHANNEL ID>\"}]" }` |
| 119 | Disconnect | Disconnect from a server | `{ T: 119 }` Request to disconnect<br> `{ T: 119, U: "<USERNAME>" }` Notification of user disconnecting |
