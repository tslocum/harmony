This document covers how to host a harmony server.

# Requirements

## Bandwidth

Clients may simultaneously transmit and receive up to four audio streams at a
rate of 64 kilobits/second.

  - Average: 64 kilobits/second * Number of users voice chatting
  - Maximum: 256 kilobits/second * Number of users voice chatting

## HTTPS

A web client is hosted from the server using insecure HTTP. Do not expose the
web client directly to the Internet. Place it behind a HTTP daemon such as
[Caddy](https://github.com/mholt/caddy), [nginx](https://www.nginx.com)
or [Apache](https://httpd.apache.org/) via
[reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy) instead.

### Caddy example

Launch harmony-server with the following configuration option:

```webaddress: localhost:19000```

Add the following to the relevant site directive in your Caddyfile:

```
proxy /w http://localhost:19000 {
        websocket
}

proxy / http://localhost:19000
```

# Installation

There are two ways to install harmony-server:

### Download

[**Download harmony-server**](https://harmony.rocketnine.space/download/?sort=name&order=desc)

### Compile

Download and install the [Go compiler](https://golang.org/doc/install), then
install harmony-server with the following command:

```GO111MODULE=on go get gitlab.com/tslocum/harmony/...```

This command will also update the server when ran again.

Execute harmony-server with the ```-help``` flag for more information:

```harmony-server -help```

If the command was not found, add ```~/go/bin``` to your
[PATH variable](https://en.wikipedia.org/wiki/PATH_%28variable%29).

# Configuration

The path to a configuration file may be specified with ```--config```, or the
default path of ```~/.config/harmony-server/config.yaml``` may be used.

Example configuration:

```
webaddress: localhost:19000
webpath: /
channels:
  Lobby-Text:
    type: text
    topic: Example text channel
  Lobby-Voice:
    type: voice
    topic: Example voice channel
  Lobby-Voice2:
    type: voice
    topic: Another voice channel
```

## Hosting the web client on a different path

The web client expects to be hosted at the root of the domain, ```/```.

To host the web client on a different path, such as ```/chat/```, set the
```webpath``` configuration option:

```webpath: /chat/```

## Hosting the web client on multiple addresses

Multiple address may be specified:

```webaddress: localhost:19000,192.168.0.101:19010```
